define(['knockout', 'css!knockoutBindings/alert_slide'], function(ko) {
    ko.bindingHandlers.alertSlide = {
        init: function(element) {
            $(element).addClass('alert-ko');
        },
        update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
            // This will be called once when the binding is first applied to an element,
            // and again whenever any observables/computeds that are accessed change
            // Update the DOM element based on the supplied values here.
            var value = valueAccessor();
            var $element = $(element);
            var $masked = null;
            var message = null;

            if (value()) {
                //Gestion du message d'erreur
                var messageReceived = ko.unwrap(value);


                switch (typeof messageReceived) {
                    case "object":
                        if (messageReceived.responseText) {
                            try {
                                message = $.parseJSON(messageReceived.responseText);
                            } catch (e) {
                                message = "<p>Une erreur est survenue</p><pre>" + messageReceived.statusText + "</pre>";
                            }
                        }
                        break;
                    case "string":
                        try {
                            message = $.parseJSON(messageReceived);
                        } catch (e) {
                            message = messageReceived;
                        }
                        break;
                    default:
                        message = "Une erreur est survenue";
                }

                //on affiche le message
                $element.html(message);

                //on place un element pour masquer le fond
                $masked = $('<div>').addClass('alert-masked');
                $('body').append($masked);
                $masked.animate({
                    opacity: 0.75
                });
                $element.animate({
                    top: "10%"
                }, 1500, function() {
                    setTimeout(function() {
                        value(null);
                    }, 3000);
                });
            } else {
                $element.html("");
                $masked = $('.alert-masked');
                $masked.animate({
                    opacity: 0
                }, function() {
                    $masked.remove();
                });
                $element.animate({
                    top: "-120px"
                });
            }
        }
    };
});