define(['knockout'], function(ko) {
    ko.bindingHandlers.opacity = {
        update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
            var value = valueAccessor();
            var $element = $(element);

            if (ko.unwrap(value)) {
                $element.animate({
                    opacity: 0.5
                });
            } else {
                $element.animate({
                    opacity: 1
                });
            }
        }
    };
});