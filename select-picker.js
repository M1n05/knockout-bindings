define(['knockout', 'bootstrap-select-fr' ], function(ko) {
    ko.bindingHandlers.selectpicker = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                if (navigator.userAgent.indexOf('MSIE') === -1) {
                    var $element = $(element);
                    $element.selectpicker();
                    $element.addClass('selectpicker');
                    if (allBindings.has('options')) {
                        var options = allBindings.get('options');
                        if (ko.isObservable(options)) {
                            options.subscribe(function() {
                                $element.selectpicker('refresh');
                            });
                        }
                    }
                }
            }
        };
});        